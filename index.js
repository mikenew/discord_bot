const https = require('https')
const request = require('request');
const config = require('./config.json')
const Discord = require('discord.js')
const client = new Discord.Client()
var Datastore = require('nedb'), db = new Datastore({ filename: 'database', autoload: true })

const cutoff = 1587031200
const day_name = "April 16"

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`)
})

client.on('message', msg => {
  if (msg.channel.name === "different-channel" || msg.channel.name === "sc2-bot-testing") {
    if (msg.content.indexOf('!add ') == 0) {
      let bnetID = msg.content.split(" ")[1]
      let guildID = msg.guild.id

      if (isNaN(bnetID)) {
        msg.reply("That doesn't look like a valid battle.net profile ID. It should be a 6 or 7 digit number.")
      } else {
        addNewUser(guildID, bnetID, msg)
      }
    } else if (msg.content.indexOf('!standings') == 0) {
      let guildID = msg.guild.id
      allUsersFor(guildID, function (users) {
        let standings = []
        var queryCounter = (users != null) ? users.length : 0
        for (i in users) {
          let user = users[i]
          getMatchHistory(guildID, user["bnetID"], function (history) {
            queryCounter -= 1
            if (queryCounter <= 0) {
              sendStats(guildID, msg)
            }
          })
        }
      })

    } else if (msg.content.indexOf('!matches') == 0) {
      let guildID = msg.guild.id
      let discordID = msg.author.id

      bnetIDFor(guildID, discordID, function (bnetID) {
        if (bnetID != null) {
          getMatchHistory(guildID, bnetID, function (history) {
            db.findOne({ guildID: guildID, discordID: discordID }, function (err, user) {
              let count = matchCountFor(user)
              msg.channel.send("<@!" + discordID + "> you've played " + count + " ladder matches since " + day_name)
            })
          })
        } else {
          msg.reply("You haven't added your battle.net profile ID. You can add it with `!add <profile_id>`")
        }
      })
    } else if (msg.content.indexOf('!remove') == 0) {
      let guildID = msg.guild.id
      let discordID = msg.author.id

      db.remove({ guildID: guildID, discordID: discordID }, {}, {}, function (err, number) {
        console.log("removed " + number + " user from the database")
      })
      msg.reply("You've been removed. You can always add yourself again with `!add <profile_id>`")
    }
  }
})


function bnetIDFor(guildID, discordID, completion) {
  db.findOne({ guildID: guildID, discordID: discordID }, function (err, doc) {
    if (doc != null) {
      if (doc["discordID"] === discordID) {
        completion(doc["bnetID"])
      }
    } else {
      completion()
    }
  })
}

function allUsersFor(guildID, completion) {
  db.find({ guildID: guildID }, function (err, docs) {
    if (docs != null) {
      completion(docs)
    } else {
      completion()
    }
  })
}

function getMatchHistory(guildID, bnetID, completion) {
  getNewBnetToken(function (token) {
    https.get('https://us.api.blizzard.com/sc2/legacy/profile/1/1/' + bnetID + '/matches?access_token=' + token, (resp) => {
      let data = ''

      resp.on('data', (chunk) => {
        data += chunk
      })

      resp.on('end', () => {
        try {
          let j = JSON.parse(data)
          saveMatchHistory(guildID, bnetID, j, function () {
            completion(j)
          })
        } catch(e) {
          console.log("error fetching battlenet data: " + data)
          completion()
        }
      })
    }).on("error", (err) => {
      console.log("Error: " + err.message)
    })
  })
}

function getNewBnetToken(completion) {
  request.post('https://' + config.battlenetClientID + ':' + config.battlenetClientSecret + '@us.battle.net/oauth/token', {
    form: { grant_type: 'client_credentials' }
  }, function (error, result, body) {
    if (error == null) {
      try {
        let j = JSON.parse(body)
        completion(j["access_token"])
      } catch(e) {
        console.log(e)
        completion()
      }
    } else {
      console.log(error)
    }
  })
}

function addNewUser(guildID, bnetID, msg) {
  db.findOne({ guildID: guildID, discordID: msg.author.id}, function (err, doc) {
    if (doc != null) {
        sg.reply("You've already added yourself! Type `!matches` to see how many matches you've played or `!standings` to see how you stack up against everyone else")
    } else {
      getMatchHistory(guildID, bnetID, function (history) {
    if (history != null) {
      db.update({ guildID: guildID, bnetID: bnetID }, { $set: { guildID: guildID, bnetID: bnetID, discordID: msg.author.id, username: msg.author.username, games : history["matches"] } }, {upsert: true}, function () { })
      msg.reply("You're now in the running! Type `!matches` to see how many matches you've played or `!standings` to see how you stack up against everyone else")
    } else {
      msg.reply("That doesn't look like a valid battle.net profile ID. If you're sure it's right it's also possible that battlenet is down.")
    }
      })
    }
  })

}

function saveMatchHistory(guildID, bnetID, matches, completion) {
  db.update({ guildID: guildID, bnetID: bnetID }, { $addToSet: { games: { $each: matches["matches"] } } }, {}, function () {
    console.log("updated games for " + bnetID)
    completion()
  })
}

function matchCountFor(user) {
  let count = 0
  for (i in user["games"]) {
    let game = user["games"][i]
    if (game['type'] === '1v1' && Number(game['date']) > cutoff) {
      count += 1
    }
  }
  return count
}

function sendStats(guildID, msg) {
  let standings = []
  allUsersFor(guildID, function (users) {
    for (i in users) {
      let user = users[i]
      standings.push(matchCountFor(users[i]) + " games played by <@!" + user["discordID"] + ">")
    }
    standings.sort()
    standings.reverse()
    if (standings.length > 0) {
      msg.channel.send(standings)
    } else {
        msg.channel.send("Couldn't get player standings right now. Battlenet might be down.")
    }
  })
}

client.login(config.discordToken)

function updateAllUsers() {
  db.find({}, function (err, users) {
    if (users != null) {
      for (i in users) {
        let user = users[i]
        getMatchHistory(user["guildID"], user["bnetID"], function (history) { })
      }
    }
  });
}

setInterval(updateAllUsers, 1800000) // 30 minutes
